#include <iostream>
#include <stdlib.h>

using namespace std;

int main()
{
    string s;
    cout << "Input String : ";
    cin >> s; // get user input from the keyboard
    
    cout << endl;
    cout << "#include <stdio.h>" << endl;
    cout << "int main()" << endl;
    cout << "{" << endl;
    cout << "\tprintf(\"%s\\n\",\""<< s;
    cout << "\");" << endl;
    cout << "\treturn 0" << endl;
    cout << "}" << endl;
    //cout << "\");"<<<<""\t()""" << firstName;
    return 0;
}