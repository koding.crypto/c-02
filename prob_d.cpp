#include <iostream>
#include <stdlib.h>

using namespace std;

int main()
{
    int n, reversedNumber = 0, remain;

    cout << "Input : ";
    cin >> n;

    while(n != 0) {
        remain = n%10;
        reversedNumber = reversedNumber*10 + remain;
        n /= 10;
    }

    cout << "Output : " << reversedNumber;

    return 0;
}