#include <iostream>

using namespace std;

int main()
{
     double fahrenheit, celsius;
 
    cout << "input the temperature in Celsius : ";
    cin >> celsius;
    fahrenheit = (celsius * 9.0) / 5.0 + 32;
    cout << fahrenheit << " .F" << endl;
    
    cout << "input the temperature in Fahrenheit : ";
    cin >> fahrenheit;
    celsius = (5.0/9.0) * (fahrenheit-32);
    cout << celsius << " .C" <<endl;

    return 0;
}