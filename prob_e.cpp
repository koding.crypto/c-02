#include <iostream>
#include <stdlib.h>

using namespace std;

int decimalToOctal(int decimalNumber);
string decimalToHex(int decimalNumber);

int main()
{
   char c;
   int decimal;
   char hex_string[20];
   
   cin >> c;
   decimal = (int)(c);
   
   cout << decimalToOctal(decimal) << " " << decimal << " " << decimalToHex(decimal) << endl;

   return 0;
}

int decimalToOctal(int decimalNumber)
{
    int rem, i = 1, octalNumber = 0;
    while (decimalNumber != 0)
    {
        rem = decimalNumber % 8;
        decimalNumber /= 8;
        octalNumber += rem * i;
        i *= 10;
    }
    return octalNumber;
}

string decimalToHex(int decimalNumber)
{
    char hex_string[20];
    sprintf(hex_string, "%X", decimalNumber);  
    
    return hex_string;
}