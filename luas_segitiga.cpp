#include <iostream>
#include <stdlib.h>

using namespace std;

int main()
{
    int x1, y1, x2, y2, alas, tinggi;
    double area;
 
    cin >> x1 >> y1 >> x2 >> y2;
    
    alas = abs(x1 - x2);
    tinggi = abs(y1 - y2);
    
    area = (alas*tinggi)/2;
    
    cout << "Area = " << area  << endl;
    
    return 0;
}