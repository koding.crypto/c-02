#include <iostream>
#include <stdlib.h>

using namespace std;

int main()
{
    int i, j, n, space, k;
    char character[9];
    
    cin >> character[0] >> character[1] >> character[2];
    cin >> character[3] >> character[4] >> character[5];
    cin >> character[6] >> character[7] >> character[8];
    cout << endl;
    
    k=0;
    n=3;
    space = n - 1;  
    for (int i = 0; i < n; i++)  
    {  
        for (int j = 0;j < space; j++)  
            cout << " ";  
  
        for (int j = 0; j <= i; j++)  {
            cout << character[k] << " ";
            k++;
        }
        cout << endl;  
        space--;  
    }  
    n=n-1;
    space = 1;  
  
    for (int i = n; i > 0; i--)  
    {  
        for (int j = 0; j < space; j++)  
            cout << " ";  
  
        for (int j = 0;j < i;j++)   {
            cout << character[k] << " ";
            k++;
        }
  
        cout << endl; 
        space++;  
    }  
    return 0;
}